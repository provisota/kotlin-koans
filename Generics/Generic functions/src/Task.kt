import java.util.*

fun <T> Collection<T>.partitionTo(trueList: MutableCollection<T>,
                                  falseList: MutableCollection<T>,
                                  predicate: (T) -> Boolean): Pair<MutableCollection<T>, MutableCollection<T>> {
    forEach {
        if (predicate(it)) {
            trueList.add(it)
        } else {
            falseList.add(it)
        }
    }
    return Pair(trueList, falseList)
}

// inline fun <T> Array<out T>.partition(predicate: (T) -> Boolean): Pair<List<T>, List<T>>
// fun <T, C : MutableCollection<in T>> Array<out T>.toCollection(destination: C): C

/*fun <T, C : MutableCollection<T>> Collection<T>.partitionTo(
        trueList: C,
        falseList: C,
        predicate: (T) -> Boolean): Pair<C, C> {
    // the same implementation
}*/

fun partitionWordsAndLines() {
    val (words, lines) = listOf("a", "a b", "c", "d e").partitionTo(ArrayList<String>(), ArrayList()) { s -> !s.contains(" ") }
    words == listOf("a", "c")
    lines == listOf("a b", "d e")
}

fun partitionLettersAndOtherSymbols() {
    val (letters, other) = setOf('a', '%', 'r', '}').partitionTo(HashSet<Char>(), HashSet()) { c -> c in 'a'..'z' || c in 'A'..'Z' }
    letters == setOf('a', 'r')
    other == setOf('%', '}')
}
